function cacheFunction(cb) {
    let cacheData = {};
    return function (...arg) {
        if (!cacheData[`${cb} of ${arg}`]) {
            cacheData[`${cb} of ${arg}`] = cb(...arg);
            return cb(...arg);
        } else {
            return cacheData[`${cb} of ${arg}`];
        }

    }

};


module.exports = cacheFunction;
