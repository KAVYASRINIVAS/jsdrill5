function counterFactory(n){
    return {
        increment(){
            return n+1;
        },
        decrement(){
            return n-1;
        }
    }
}

module.exports = counterFactory;

/*
const counter=counterFactory(5);
console.log(counter.increment());
console.log(counter.decrement());

const counter1=counterFactory(15);
console.log(counter1.increment());
console.log(counter1.decrement());
*/