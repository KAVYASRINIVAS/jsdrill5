function limitFunctionCount(n, cb) {
    let count = 0;
    return {
        callLimit() {
            count++;
            if (count <= n) {
                return cb(count);
            }else{
                return null;
            }
        }
    }
}

module.exports = limitFunctionCount;

/*
const limit=limitFunctionCount(5,callingFunction);
console.log(limit.callLimit());
console.log(limit.callLimit());
console.log(limit.callLimit());
console.log(limit.callLimit());
console.log(limit.callLimit());
console.log(limit.callLimit());
*/